import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import * as config from '../../config';

@Injectable({
  providedIn: 'root'
})
export class DriversService {
  SERVER_BACK = config.environment.BASE_URL.SERVER_BACK;

  constructor(private  http: HttpClient) { }

  getdriver(idDriver:string) {
    let headers = new HttpHeaders({'content-Type': 'application/json'});
    return this.http.get(this.SERVER_BACK + '/driver/getdriver/'+idDriver,{headers});
  }
}
