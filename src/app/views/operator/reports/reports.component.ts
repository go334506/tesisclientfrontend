import { Component, OnInit } from '@angular/core';
import {ReportsService} from '../../../services/reports.service';

@Component({
  selector: 'app-reports',
  templateUrl: './reports.component.html',
  styleUrls: ['./reports.component.css']
})
export class ReportsComponent implements OnInit {
  reportes: any = [];
  nReportes: number;
  loadReports: boolean=false;
  constructor(private reportsService: ReportsService) {
    (async () => {
      await this.serviceloadReports();
    })();
  }

  async ngOnInit() {
    setInterval(() => { this.serviceloadReports(); }, 1000);

  }

  async serviceloadReports() {
    this.reportsService.getReporters().subscribe(
      async res => {
        await res;
        this.reportes = res;
        if(this.reportes.status.code){
          this.reportes = this.reportes.reports;
          this.nReportes = this.reportes.length;
          this.loadReports = true;
        }
        console.log(JSON.stringify(this.reportes));
      },
      err => console.error(err)
    );
  }

}
