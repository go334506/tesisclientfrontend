import { Component, OnInit } from '@angular/core';
import {ReportsService} from '../../../services/reports.service';
import {DriversService} from '../../../services/drivers.service';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-report-details',
  templateUrl: './report-details.component.html',
  styleUrls: ['./report-details.component.css']
})
export class ReportDetailsComponent implements OnInit {
  loadInfo: boolean= false;
  driverInfo: any;
  driverMedical: any = [];
  driverVehicle: any = [];
  idDriver: string;
  nameDriver:string;
  constructor(private driversService: DriversService, private route: ActivatedRoute) {
    (async () => {
      this.idDriver = this.route.snapshot.paramMap.get('id');
      await this.servicedriverInfo(this.idDriver);
    })();
  }

 async ngOnInit() {

  }

  async servicedriverInfo(idDriver:string) {
    this.driversService.getdriver(idDriver).subscribe(
      async res => {
        await res;
        this.driverInfo = res;
        if(this.driverInfo.status.code){
          this.driverInfo = this.driverInfo.driver;
          this.loadInfo = true;
          this.nameDriver = this.driverInfo.appat + ' ' + this.driverInfo.amat + ' '+this.driverInfo.name;

        }
        console.log(JSON.stringify(this.driverInfo));
      },
      err => console.error(err)
    );
  }
}
