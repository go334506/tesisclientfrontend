import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './views/login/login.component';
import { ReportsComponent } from './views/operator/reports/reports.component';
import { ReportDetailsComponent } from './views/operator/report-details/report-details.component';

const routes: Routes = [
  {
    path: '',
    component : LoginComponent
  },
  {
    path: 'operador/reportes',
    component : ReportsComponent
  },
  {
    path: 'operador/reportes/detalles/:id',
    component : ReportDetailsComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
